import React, { Component } from 'react';
import './App.css';
import Machine from './Machine/Machine';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bankNotes: { 100: 0, 50: 0, 20: 0, 10: 0 },
            errorMessage: null
        };
        this.updateState = this.updateState.bind(this);
        this.printErrorMessage = this.printErrorMessage.bind(this);
    }

    /**
     * Receives new state from child component and updates states for this component
     * @param newState -- State to update to
     */
    updateState(newState) {
        this.setState({
            bankNotes: newState,
            errorMessage: null
        });
    }

    /**
     * Receives error message from child component and shows it to user
     * @param message -- Message to show to user
     */
    printErrorMessage(message) {
        this.setState({
            bankNotes: { 100: 0, 50: 0, 20: 0, 10: 0 },
            errorMessage: message
        });
    }
    render() {
        return (
            <div className="App">
                <Machine
                    oncalcCash={this.updateState}
                    onPrintErrorMessage={this.printErrorMessage}
                />
                <h2 id="error-message" className="error-messages">
                    {' '}
                    {this.state.errorMessage}
                </h2>
                <span className="bank-note">
                    $100: {this.state.bankNotes['100']}{' '}
                </span>
                <span className="bank-note">
                    $50: {this.state.bankNotes['50']}{' '}
                </span>
                <span className="bank-note">
                    $20: {this.state.bankNotes['20']}{' '}
                </span>
                <span className="bank-note">
                    $10: {this.state.bankNotes['10']}
                </span>
            </div>
        );
    }
}
