import React from 'react';
import App from './App';
import { shallow } from 'enzyme';

it('renders error message', () => {
    const wrapper = shallow(<App />);
    wrapper.instance().printErrorMessage('error');
    const message = wrapper.instance().state.errorMessage;
    expect(message).toEqual('error');
});
it('updates state correctly', () => {
    const wrapper = shallow(<App />);
    wrapper.instance().updateState({ 100: 0, 50: 1, 20: 1, 10: 0 });
    expect(wrapper.instance().state.bankNotes).toEqual({
        100: 0,
        50: 1,
        20: 1,
        10: 0
    });
});
