import React, { Component } from 'react';
import './Machine.css';

export default class Machine extends Component {
    constructor(props) {
        super(props);
        this.calcCash = this.calcCash.bind(this);
    }

    /**
     * listens to event from input box and if it's the Enter key calculates the change that should be returned
     * @param e -- event
     */
    calcCash(e) {
        let notes = { 100: 0, 50: 0, 20: 0, 10: 0 },
            amount = null;
        if (e.key === 'Enter') {
            amount = e.target.value;
            // if amount is less than zero or it is a string, then print error message
            if (amount <= 0 || isNaN(amount)) {
                return this.props.onPrintErrorMessage(
                    'Not unavailable exception'
                );
            }
        }

        [100, 50, 20, 10].forEach(note => {
            if (amount >= note) {
                notes[note] = Math.floor(amount / note);
                amount = amount - notes[note] * note;
            }
        });
        // if amount is still bigger than 0 means that the machine cannot give change
        amount > 0
            ? this.props.onPrintErrorMessage(
                  'Change not available at the moment'
              )
            : this.props.oncalcCash(notes);
    }
    render() {
        return (
            <div className="cashInput-wrapper">
                <label className="cashInputText">
                    Amount in <span className="cashSign">$</span>{' '}
                    <input
                        id="target-input"
                        className="cashInput"
                        type="text"
                        onKeyPress={this.calcCash}
                    />
                </label>
            </div>
        );
    }
}
